// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web;

import com.nw.domain.ReserveInfo;
import com.nw.web.ReserveInfoBean;
import com.nw.web.util.MessageFactory;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.message.Message;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.spinner.Spinner;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

privileged aspect ReserveInfoBean_Roo_ManagedBean {
    
    declare @type: ReserveInfoBean: @ManagedBean(name = "reserveInfoBean");
    
    declare @type: ReserveInfoBean: @SessionScoped;
    
    private String ReserveInfoBean.name = "ReserveInfoes";
    
    private ReserveInfo ReserveInfoBean.reserveInfo;
    
    private List<ReserveInfo> ReserveInfoBean.allReserveInfoes;
    
    private boolean ReserveInfoBean.dataVisible = false;
    
    private List<String> ReserveInfoBean.columns;
    
    private HtmlPanelGrid ReserveInfoBean.createPanelGrid;
    
    private HtmlPanelGrid ReserveInfoBean.editPanelGrid;
    
    private HtmlPanelGrid ReserveInfoBean.viewPanelGrid;
    
    private boolean ReserveInfoBean.createDialogVisible = false;
    
    @PostConstruct
    public void ReserveInfoBean.init() {
        columns = new ArrayList<String>();
        columns.add("maxReserve");
        columns.add("minReserve");
        columns.add("usageQuotaThreshold");
    }
    
    public String ReserveInfoBean.getName() {
        return name;
    }
    
    public List<String> ReserveInfoBean.getColumns() {
        return columns;
    }
    
    public List<ReserveInfo> ReserveInfoBean.getAllReserveInfoes() {
        return allReserveInfoes;
    }
    
    public void ReserveInfoBean.setAllReserveInfoes(List<ReserveInfo> allReserveInfoes) {
        this.allReserveInfoes = allReserveInfoes;
    }
    
    public String ReserveInfoBean.findAllReserveInfoes() {
        allReserveInfoes = ReserveInfo.findAllReserveInfoes();
        dataVisible = !allReserveInfoes.isEmpty();
        return null;
    }
    
    public boolean ReserveInfoBean.isDataVisible() {
        return dataVisible;
    }
    
    public void ReserveInfoBean.setDataVisible(boolean dataVisible) {
        this.dataVisible = dataVisible;
    }
    
    public HtmlPanelGrid ReserveInfoBean.getCreatePanelGrid() {
        if (createPanelGrid == null) {
            createPanelGrid = populateCreatePanel();
        }
        return createPanelGrid;
    }
    
    public void ReserveInfoBean.setCreatePanelGrid(HtmlPanelGrid createPanelGrid) {
        this.createPanelGrid = createPanelGrid;
    }
    
    public HtmlPanelGrid ReserveInfoBean.getEditPanelGrid() {
        if (editPanelGrid == null) {
            editPanelGrid = populateEditPanel();
        }
        return editPanelGrid;
    }
    
    public void ReserveInfoBean.setEditPanelGrid(HtmlPanelGrid editPanelGrid) {
        this.editPanelGrid = editPanelGrid;
    }
    
    public HtmlPanelGrid ReserveInfoBean.getViewPanelGrid() {
        return populateViewPanel();
    }
    
    public void ReserveInfoBean.setViewPanelGrid(HtmlPanelGrid viewPanelGrid) {
        this.viewPanelGrid = viewPanelGrid;
    }
    
    public HtmlPanelGrid ReserveInfoBean.populateCreatePanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel maxReserveCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        maxReserveCreateOutput.setFor("maxReserveCreateInput");
        maxReserveCreateOutput.setId("maxReserveCreateOutput");
        maxReserveCreateOutput.setValue("Max Reserve:");
        htmlPanelGrid.getChildren().add(maxReserveCreateOutput);
        
        Spinner maxReserveCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        maxReserveCreateInput.setId("maxReserveCreateInput");
        maxReserveCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.maxReserve}", Integer.class));
        maxReserveCreateInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(maxReserveCreateInput);
        
        Message maxReserveCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        maxReserveCreateInputMessage.setId("maxReserveCreateInputMessage");
        maxReserveCreateInputMessage.setFor("maxReserveCreateInput");
        maxReserveCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(maxReserveCreateInputMessage);
        
        OutputLabel minReserveCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        minReserveCreateOutput.setFor("minReserveCreateInput");
        minReserveCreateOutput.setId("minReserveCreateOutput");
        minReserveCreateOutput.setValue("Min Reserve:");
        htmlPanelGrid.getChildren().add(minReserveCreateOutput);
        
        Spinner minReserveCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        minReserveCreateInput.setId("minReserveCreateInput");
        minReserveCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.minReserve}", Integer.class));
        minReserveCreateInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(minReserveCreateInput);
        
        Message minReserveCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        minReserveCreateInputMessage.setId("minReserveCreateInputMessage");
        minReserveCreateInputMessage.setFor("minReserveCreateInput");
        minReserveCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(minReserveCreateInputMessage);
        
        OutputLabel usageQuotaThresholdCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        usageQuotaThresholdCreateOutput.setFor("usageQuotaThresholdCreateInput");
        usageQuotaThresholdCreateOutput.setId("usageQuotaThresholdCreateOutput");
        usageQuotaThresholdCreateOutput.setValue("Usage Quota Threshold:");
        htmlPanelGrid.getChildren().add(usageQuotaThresholdCreateOutput);
        
        Spinner usageQuotaThresholdCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        usageQuotaThresholdCreateInput.setId("usageQuotaThresholdCreateInput");
        usageQuotaThresholdCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.usageQuotaThreshold}", Integer.class));
        usageQuotaThresholdCreateInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(usageQuotaThresholdCreateInput);
        
        Message usageQuotaThresholdCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        usageQuotaThresholdCreateInputMessage.setId("usageQuotaThresholdCreateInputMessage");
        usageQuotaThresholdCreateInputMessage.setFor("usageQuotaThresholdCreateInput");
        usageQuotaThresholdCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(usageQuotaThresholdCreateInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid ReserveInfoBean.populateEditPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel maxReserveEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        maxReserveEditOutput.setFor("maxReserveEditInput");
        maxReserveEditOutput.setId("maxReserveEditOutput");
        maxReserveEditOutput.setValue("Max Reserve:");
        htmlPanelGrid.getChildren().add(maxReserveEditOutput);
        
        Spinner maxReserveEditInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        maxReserveEditInput.setId("maxReserveEditInput");
        maxReserveEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.maxReserve}", Integer.class));
        maxReserveEditInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(maxReserveEditInput);
        
        Message maxReserveEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        maxReserveEditInputMessage.setId("maxReserveEditInputMessage");
        maxReserveEditInputMessage.setFor("maxReserveEditInput");
        maxReserveEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(maxReserveEditInputMessage);
        
        OutputLabel minReserveEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        minReserveEditOutput.setFor("minReserveEditInput");
        minReserveEditOutput.setId("minReserveEditOutput");
        minReserveEditOutput.setValue("Min Reserve:");
        htmlPanelGrid.getChildren().add(minReserveEditOutput);
        
        Spinner minReserveEditInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        minReserveEditInput.setId("minReserveEditInput");
        minReserveEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.minReserve}", Integer.class));
        minReserveEditInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(minReserveEditInput);
        
        Message minReserveEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        minReserveEditInputMessage.setId("minReserveEditInputMessage");
        minReserveEditInputMessage.setFor("minReserveEditInput");
        minReserveEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(minReserveEditInputMessage);
        
        OutputLabel usageQuotaThresholdEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        usageQuotaThresholdEditOutput.setFor("usageQuotaThresholdEditInput");
        usageQuotaThresholdEditOutput.setId("usageQuotaThresholdEditOutput");
        usageQuotaThresholdEditOutput.setValue("Usage Quota Threshold:");
        htmlPanelGrid.getChildren().add(usageQuotaThresholdEditOutput);
        
        Spinner usageQuotaThresholdEditInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        usageQuotaThresholdEditInput.setId("usageQuotaThresholdEditInput");
        usageQuotaThresholdEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.usageQuotaThreshold}", Integer.class));
        usageQuotaThresholdEditInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(usageQuotaThresholdEditInput);
        
        Message usageQuotaThresholdEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        usageQuotaThresholdEditInputMessage.setId("usageQuotaThresholdEditInputMessage");
        usageQuotaThresholdEditInputMessage.setFor("usageQuotaThresholdEditInput");
        usageQuotaThresholdEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(usageQuotaThresholdEditInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid ReserveInfoBean.populateViewPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        HtmlOutputText maxReserveLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        maxReserveLabel.setId("maxReserveLabel");
        maxReserveLabel.setValue("Max Reserve:");
        htmlPanelGrid.getChildren().add(maxReserveLabel);
        
        HtmlOutputText maxReserveValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        maxReserveValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.maxReserve}", String.class));
        htmlPanelGrid.getChildren().add(maxReserveValue);
        
        HtmlOutputText minReserveLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        minReserveLabel.setId("minReserveLabel");
        minReserveLabel.setValue("Min Reserve:");
        htmlPanelGrid.getChildren().add(minReserveLabel);
        
        HtmlOutputText minReserveValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        minReserveValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.minReserve}", String.class));
        htmlPanelGrid.getChildren().add(minReserveValue);
        
        HtmlOutputText usageQuotaThresholdLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        usageQuotaThresholdLabel.setId("usageQuotaThresholdLabel");
        usageQuotaThresholdLabel.setValue("Usage Quota Threshold:");
        htmlPanelGrid.getChildren().add(usageQuotaThresholdLabel);
        
        HtmlOutputText usageQuotaThresholdValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        usageQuotaThresholdValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{reserveInfoBean.reserveInfo.usageQuotaThreshold}", String.class));
        htmlPanelGrid.getChildren().add(usageQuotaThresholdValue);
        
        return htmlPanelGrid;
    }
    
    public ReserveInfo ReserveInfoBean.getReserveInfo() {
        if (reserveInfo == null) {
            reserveInfo = new ReserveInfo();
        }
        return reserveInfo;
    }
    
    public void ReserveInfoBean.setReserveInfo(ReserveInfo reserveInfo) {
        this.reserveInfo = reserveInfo;
    }
    
    public String ReserveInfoBean.onEdit() {
        return null;
    }
    
    public boolean ReserveInfoBean.isCreateDialogVisible() {
        return createDialogVisible;
    }
    
    public void ReserveInfoBean.setCreateDialogVisible(boolean createDialogVisible) {
        this.createDialogVisible = createDialogVisible;
    }
    
    public String ReserveInfoBean.displayList() {
        createDialogVisible = false;
        findAllReserveInfoes();
        return "reserveInfo";
    }
    
    public String ReserveInfoBean.displayCreateDialog() {
        reserveInfo = new ReserveInfo();
        createDialogVisible = true;
        return "reserveInfo";
    }
    
    public String ReserveInfoBean.persist() {
        String message = "";
        if (reserveInfo.getReserveInfoId() != null) {
            reserveInfo.merge();
            message = "message_successfully_updated";
        } else {
            reserveInfo.persist();
            message = "message_successfully_created";
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("createDialogWidget.hide()");
        context.execute("editDialogWidget.hide()");
        
        FacesMessage facesMessage = MessageFactory.getMessage(message, "ReserveInfo");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllReserveInfoes();
    }
    
    public String ReserveInfoBean.delete() {
        reserveInfo.remove();
        FacesMessage facesMessage = MessageFactory.getMessage("message_successfully_deleted", "ReserveInfo");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllReserveInfoes();
    }
    
    public void ReserveInfoBean.reset() {
        reserveInfo = null;
        createDialogVisible = false;
    }
    
    public void ReserveInfoBean.handleDialogClose(CloseEvent event) {
        reset();
    }
    
}
