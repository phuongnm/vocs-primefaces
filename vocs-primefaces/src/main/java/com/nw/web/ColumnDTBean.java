package com.nw.web;
import com.nw.domain.ColumnDT;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ColumnDT.class, beanName = "columnDTBean")
public class ColumnDTBean {
}
