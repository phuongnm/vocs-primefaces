package com.nw.web;
import com.nw.domain.ZoneData;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ZoneData.class, beanName = "zoneDataBean")
public class ZoneDataBean {
}
