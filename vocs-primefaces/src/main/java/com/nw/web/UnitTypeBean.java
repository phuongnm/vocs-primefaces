package com.nw.web;
import com.nw.domain.UnitType;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = UnitType.class, beanName = "unitTypeBean")
public class UnitTypeBean {
}
