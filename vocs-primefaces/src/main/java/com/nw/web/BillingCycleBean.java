package com.nw.web;
import com.nw.domain.BillingCycle;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = BillingCycle.class, beanName = "billingCycleBean")
public class BillingCycleBean {
}
