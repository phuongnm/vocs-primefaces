package com.nw.web;
import com.nw.domain.NormParam;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = NormParam.class, beanName = "normParamBean")
public class NormParamBean {
}
