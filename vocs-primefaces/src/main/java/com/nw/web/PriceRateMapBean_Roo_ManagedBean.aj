// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web;

import com.nw.domain.PriceComponent;
import com.nw.domain.PriceRateMap;
import com.nw.domain.RateTable;
import com.nw.web.PriceRateMapBean;
import com.nw.web.converter.PriceComponentConverter;
import com.nw.web.converter.RateTableConverter;
import com.nw.web.util.MessageFactory;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.message.Message;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

privileged aspect PriceRateMapBean_Roo_ManagedBean {
    
    declare @type: PriceRateMapBean: @ManagedBean(name = "priceRateMapBean");
    
    declare @type: PriceRateMapBean: @SessionScoped;
    
    private String PriceRateMapBean.name = "PriceRateMaps";
    
    private PriceRateMap PriceRateMapBean.priceRateMap;
    
    private List<PriceRateMap> PriceRateMapBean.allPriceRateMaps;
    
    private boolean PriceRateMapBean.dataVisible = false;
    
    private List<String> PriceRateMapBean.columns;
    
    private HtmlPanelGrid PriceRateMapBean.createPanelGrid;
    
    private HtmlPanelGrid PriceRateMapBean.editPanelGrid;
    
    private HtmlPanelGrid PriceRateMapBean.viewPanelGrid;
    
    private boolean PriceRateMapBean.createDialogVisible = false;
    
    @PostConstruct
    public void PriceRateMapBean.init() {
        columns = new ArrayList<String>();
    }
    
    public String PriceRateMapBean.getName() {
        return name;
    }
    
    public List<String> PriceRateMapBean.getColumns() {
        return columns;
    }
    
    public List<PriceRateMap> PriceRateMapBean.getAllPriceRateMaps() {
        return allPriceRateMaps;
    }
    
    public void PriceRateMapBean.setAllPriceRateMaps(List<PriceRateMap> allPriceRateMaps) {
        this.allPriceRateMaps = allPriceRateMaps;
    }
    
    public String PriceRateMapBean.findAllPriceRateMaps() {
        allPriceRateMaps = PriceRateMap.findAllPriceRateMaps();
        dataVisible = !allPriceRateMaps.isEmpty();
        return null;
    }
    
    public boolean PriceRateMapBean.isDataVisible() {
        return dataVisible;
    }
    
    public void PriceRateMapBean.setDataVisible(boolean dataVisible) {
        this.dataVisible = dataVisible;
    }
    
    public HtmlPanelGrid PriceRateMapBean.getCreatePanelGrid() {
        if (createPanelGrid == null) {
            createPanelGrid = populateCreatePanel();
        }
        return createPanelGrid;
    }
    
    public void PriceRateMapBean.setCreatePanelGrid(HtmlPanelGrid createPanelGrid) {
        this.createPanelGrid = createPanelGrid;
    }
    
    public HtmlPanelGrid PriceRateMapBean.getEditPanelGrid() {
        if (editPanelGrid == null) {
            editPanelGrid = populateEditPanel();
        }
        return editPanelGrid;
    }
    
    public void PriceRateMapBean.setEditPanelGrid(HtmlPanelGrid editPanelGrid) {
        this.editPanelGrid = editPanelGrid;
    }
    
    public HtmlPanelGrid PriceRateMapBean.getViewPanelGrid() {
        return populateViewPanel();
    }
    
    public void PriceRateMapBean.setViewPanelGrid(HtmlPanelGrid viewPanelGrid) {
        this.viewPanelGrid = viewPanelGrid;
    }
    
    public HtmlPanelGrid PriceRateMapBean.populateCreatePanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel priceComponentIdCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        priceComponentIdCreateOutput.setFor("priceComponentIdCreateInput");
        priceComponentIdCreateOutput.setId("priceComponentIdCreateOutput");
        priceComponentIdCreateOutput.setValue("Price Component Id:");
        htmlPanelGrid.getChildren().add(priceComponentIdCreateOutput);
        
        AutoComplete priceComponentIdCreateInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        priceComponentIdCreateInput.setId("priceComponentIdCreateInput");
        priceComponentIdCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{priceRateMapBean.priceRateMap.priceComponentId}", PriceComponent.class));
        priceComponentIdCreateInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{priceRateMapBean.completePriceComponentId}", List.class, new Class[] { String.class }));
        priceComponentIdCreateInput.setDropdown(true);
        priceComponentIdCreateInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "priceComponentId", String.class));
        priceComponentIdCreateInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{priceComponentId.priceComponentName} #{priceComponentId.priceComponentId}", String.class));
        priceComponentIdCreateInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{priceComponentId}", PriceComponent.class));
        priceComponentIdCreateInput.setConverter(new PriceComponentConverter());
        priceComponentIdCreateInput.setRequired(false);
        htmlPanelGrid.getChildren().add(priceComponentIdCreateInput);
        
        Message priceComponentIdCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        priceComponentIdCreateInputMessage.setId("priceComponentIdCreateInputMessage");
        priceComponentIdCreateInputMessage.setFor("priceComponentIdCreateInput");
        priceComponentIdCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(priceComponentIdCreateInputMessage);
        
        OutputLabel rateTableIdCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        rateTableIdCreateOutput.setFor("rateTableIdCreateInput");
        rateTableIdCreateOutput.setId("rateTableIdCreateOutput");
        rateTableIdCreateOutput.setValue("Rate Table Id:");
        htmlPanelGrid.getChildren().add(rateTableIdCreateOutput);
        
        AutoComplete rateTableIdCreateInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        rateTableIdCreateInput.setId("rateTableIdCreateInput");
        rateTableIdCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{priceRateMapBean.priceRateMap.rateTableId}", RateTable.class));
        rateTableIdCreateInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{priceRateMapBean.completeRateTableId}", List.class, new Class[] { String.class }));
        rateTableIdCreateInput.setDropdown(true);
        rateTableIdCreateInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "rateTableId", String.class));
        rateTableIdCreateInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{rateTableId.rateTableName} #{rateTableId.effDate} #{rateTableId.expDate} #{rateTableId.remark}", String.class));
        rateTableIdCreateInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{rateTableId}", RateTable.class));
        rateTableIdCreateInput.setConverter(new RateTableConverter());
        rateTableIdCreateInput.setRequired(false);
        htmlPanelGrid.getChildren().add(rateTableIdCreateInput);
        
        Message rateTableIdCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        rateTableIdCreateInputMessage.setId("rateTableIdCreateInputMessage");
        rateTableIdCreateInputMessage.setFor("rateTableIdCreateInput");
        rateTableIdCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(rateTableIdCreateInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid PriceRateMapBean.populateEditPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel priceComponentIdEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        priceComponentIdEditOutput.setFor("priceComponentIdEditInput");
        priceComponentIdEditOutput.setId("priceComponentIdEditOutput");
        priceComponentIdEditOutput.setValue("Price Component Id:");
        htmlPanelGrid.getChildren().add(priceComponentIdEditOutput);
        
        AutoComplete priceComponentIdEditInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        priceComponentIdEditInput.setId("priceComponentIdEditInput");
        priceComponentIdEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{priceRateMapBean.priceRateMap.priceComponentId}", PriceComponent.class));
        priceComponentIdEditInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{priceRateMapBean.completePriceComponentId}", List.class, new Class[] { String.class }));
        priceComponentIdEditInput.setDropdown(true);
        priceComponentIdEditInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "priceComponentId", String.class));
        priceComponentIdEditInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{priceComponentId.priceComponentName} #{priceComponentId.priceComponentId}", String.class));
        priceComponentIdEditInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{priceComponentId}", PriceComponent.class));
        priceComponentIdEditInput.setConverter(new PriceComponentConverter());
        priceComponentIdEditInput.setRequired(false);
        htmlPanelGrid.getChildren().add(priceComponentIdEditInput);
        
        Message priceComponentIdEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        priceComponentIdEditInputMessage.setId("priceComponentIdEditInputMessage");
        priceComponentIdEditInputMessage.setFor("priceComponentIdEditInput");
        priceComponentIdEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(priceComponentIdEditInputMessage);
        
        OutputLabel rateTableIdEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        rateTableIdEditOutput.setFor("rateTableIdEditInput");
        rateTableIdEditOutput.setId("rateTableIdEditOutput");
        rateTableIdEditOutput.setValue("Rate Table Id:");
        htmlPanelGrid.getChildren().add(rateTableIdEditOutput);
        
        AutoComplete rateTableIdEditInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        rateTableIdEditInput.setId("rateTableIdEditInput");
        rateTableIdEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{priceRateMapBean.priceRateMap.rateTableId}", RateTable.class));
        rateTableIdEditInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{priceRateMapBean.completeRateTableId}", List.class, new Class[] { String.class }));
        rateTableIdEditInput.setDropdown(true);
        rateTableIdEditInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "rateTableId", String.class));
        rateTableIdEditInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{rateTableId.rateTableName} #{rateTableId.effDate} #{rateTableId.expDate} #{rateTableId.remark}", String.class));
        rateTableIdEditInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{rateTableId}", RateTable.class));
        rateTableIdEditInput.setConverter(new RateTableConverter());
        rateTableIdEditInput.setRequired(false);
        htmlPanelGrid.getChildren().add(rateTableIdEditInput);
        
        Message rateTableIdEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        rateTableIdEditInputMessage.setId("rateTableIdEditInputMessage");
        rateTableIdEditInputMessage.setFor("rateTableIdEditInput");
        rateTableIdEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(rateTableIdEditInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid PriceRateMapBean.populateViewPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        HtmlOutputText priceComponentIdLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        priceComponentIdLabel.setId("priceComponentIdLabel");
        priceComponentIdLabel.setValue("Price Component Id:");
        htmlPanelGrid.getChildren().add(priceComponentIdLabel);
        
        HtmlOutputText priceComponentIdValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        priceComponentIdValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{priceRateMapBean.priceRateMap.priceComponentId}", PriceComponent.class));
        priceComponentIdValue.setConverter(new PriceComponentConverter());
        htmlPanelGrid.getChildren().add(priceComponentIdValue);
        
        HtmlOutputText rateTableIdLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        rateTableIdLabel.setId("rateTableIdLabel");
        rateTableIdLabel.setValue("Rate Table Id:");
        htmlPanelGrid.getChildren().add(rateTableIdLabel);
        
        HtmlOutputText rateTableIdValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        rateTableIdValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{priceRateMapBean.priceRateMap.rateTableId}", RateTable.class));
        rateTableIdValue.setConverter(new RateTableConverter());
        htmlPanelGrid.getChildren().add(rateTableIdValue);
        
        return htmlPanelGrid;
    }
    
    public PriceRateMap PriceRateMapBean.getPriceRateMap() {
        if (priceRateMap == null) {
            priceRateMap = new PriceRateMap();
        }
        return priceRateMap;
    }
    
    public void PriceRateMapBean.setPriceRateMap(PriceRateMap priceRateMap) {
        this.priceRateMap = priceRateMap;
    }
    
    public List<PriceComponent> PriceRateMapBean.completePriceComponentId(String query) {
        List<PriceComponent> suggestions = new ArrayList<PriceComponent>();
        for (PriceComponent priceComponent : PriceComponent.findAllPriceComponents()) {
            String priceComponentStr = String.valueOf(priceComponent.getPriceComponentName() +  " "  + priceComponent.getPriceComponentId());
            if (priceComponentStr.toLowerCase().startsWith(query.toLowerCase())) {
                suggestions.add(priceComponent);
            }
        }
        return suggestions;
    }
    
    public List<RateTable> PriceRateMapBean.completeRateTableId(String query) {
        List<RateTable> suggestions = new ArrayList<RateTable>();
        for (RateTable rateTable : RateTable.findAllRateTables()) {
            String rateTableStr = String.valueOf(rateTable.getRateTableName() +  " "  + rateTable.getEffDate() +  " "  + rateTable.getExpDate() +  " "  + rateTable.getRemark());
            if (rateTableStr.toLowerCase().startsWith(query.toLowerCase())) {
                suggestions.add(rateTable);
            }
        }
        return suggestions;
    }
    
    public String PriceRateMapBean.onEdit() {
        return null;
    }
    
    public boolean PriceRateMapBean.isCreateDialogVisible() {
        return createDialogVisible;
    }
    
    public void PriceRateMapBean.setCreateDialogVisible(boolean createDialogVisible) {
        this.createDialogVisible = createDialogVisible;
    }
    
    public String PriceRateMapBean.displayList() {
        createDialogVisible = false;
        findAllPriceRateMaps();
        return "priceRateMap";
    }
    
    public String PriceRateMapBean.displayCreateDialog() {
        priceRateMap = new PriceRateMap();
        createDialogVisible = true;
        return "priceRateMap";
    }
    
    public String PriceRateMapBean.persist() {
        String message = "";
        if (priceRateMap.getPriceRateMapId() != null) {
            priceRateMap.merge();
            message = "message_successfully_updated";
        } else {
            priceRateMap.persist();
            message = "message_successfully_created";
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("createDialogWidget.hide()");
        context.execute("editDialogWidget.hide()");
        
        FacesMessage facesMessage = MessageFactory.getMessage(message, "PriceRateMap");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllPriceRateMaps();
    }
    
    public String PriceRateMapBean.delete() {
        priceRateMap.remove();
        FacesMessage facesMessage = MessageFactory.getMessage("message_successfully_deleted", "PriceRateMap");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllPriceRateMaps();
    }
    
    public void PriceRateMapBean.reset() {
        priceRateMap = null;
        createDialogVisible = false;
    }
    
    public void PriceRateMapBean.handleDialogClose(CloseEvent event) {
        reset();
    }
    
}
