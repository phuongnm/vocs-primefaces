package com.nw.web;
import com.nw.domain.Parameters;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Parameters.class, beanName = "parametersBean")
public class ParametersBean {
}
