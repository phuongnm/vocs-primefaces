package com.nw.web;
import com.nw.domain.Threshold;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Threshold.class, beanName = "thresholdBean")
public class ThresholdBean {
}
