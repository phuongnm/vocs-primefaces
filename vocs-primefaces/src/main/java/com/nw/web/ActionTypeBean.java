package com.nw.web;
import com.nw.domain.ActionType;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ActionType.class, beanName = "actionTypeBean")
public class ActionTypeBean {
}
