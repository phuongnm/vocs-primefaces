package com.nw.web;
import com.nw.domain.TriggerEventType;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerEventType.class, beanName = "triggerEventTypeBean")
public class TriggerEventTypeBean {
}
