package com.nw.web;
import com.nw.domain.PriceRateMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = PriceRateMap.class, beanName = "priceRateMapBean")
public class PriceRateMapBean {
}
