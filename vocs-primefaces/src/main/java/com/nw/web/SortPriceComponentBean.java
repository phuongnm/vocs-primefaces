package com.nw.web;
import com.nw.domain.SortPriceComponent;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = SortPriceComponent.class, beanName = "sortPriceComponentBean")
public class SortPriceComponentBean {
}
