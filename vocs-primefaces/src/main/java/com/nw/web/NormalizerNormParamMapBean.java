package com.nw.web;
import com.nw.domain.NormalizerNormParamMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = NormalizerNormParamMap.class, beanName = "normalizerNormParamMapBean")
public class NormalizerNormParamMapBean {
}
