package com.nw.web;
import com.nw.domain.TriggerFieldFormat;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerFieldFormat.class, beanName = "triggerFieldFormatBean")
public class TriggerFieldFormatBean {
}
