package com.nw.web;
import com.nw.domain.EventActionMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = EventActionMap.class, beanName = "eventActionMapBean")
public class EventActionMapBean {
}
