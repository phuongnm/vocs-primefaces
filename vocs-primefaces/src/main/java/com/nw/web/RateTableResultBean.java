package com.nw.web;
import com.nw.domain.RateTableResult;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = RateTableResult.class, beanName = "rateTableResultBean")
public class RateTableResultBean {
}
