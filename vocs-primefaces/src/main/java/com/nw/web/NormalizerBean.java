package com.nw.web;
import java.util.Date;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;

import com.nw.domain.Category;
import com.nw.domain.Normalizer;
import com.nw.web.converter.CategoryConverter;

import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.column.Column;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.message.Message;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.row.Row;
import org.primefaces.component.spinner.Spinner;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Normalizer.class, beanName = "normalizerBean")
public class NormalizerBean {

	public HtmlPanelGrid populateCreatePanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        
        HtmlPanelGrid htmlPanelGrid1 = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        HtmlPanelGrid htmlPanelGrid2 = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        htmlPanelGrid.setColumns(2);
        htmlPanelGrid.getChildren().add(htmlPanelGrid1);
        htmlPanelGrid.getChildren().add(htmlPanelGrid2);
        
        HtmlDataTable datatable=(HtmlDataTable) application.createComponent(HtmlDataTable.COMPONENT_TYPE);
        datatable.setTitle("adfadfadfda");
        Row r1 = new Row();
        Column col1 = new Column();
        col1.setColspan(2);
        
        
        col1.getChildren().add(datatable );
       
        
        htmlPanelGrid.getChildren().add(htmlPanelGrid1);
        htmlPanelGrid.getChildren().add(htmlPanelGrid2);
        htmlPanelGrid.getChildren().add(col1);
        
        OutputLabel normalizerNameCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        normalizerNameCreateOutput.setFor("normalizerNameCreateInput");
        normalizerNameCreateOutput.setId("normalizerNameCreateOutput");
        normalizerNameCreateOutput.setValue("Normalizer Name:");
        htmlPanelGrid1.getChildren().add(normalizerNameCreateOutput);
        
        InputText normalizerNameCreateInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        normalizerNameCreateInput.setId("normalizerNameCreateInput");
        normalizerNameCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.normalizerName}", String.class));
        normalizerNameCreateInput.setRequired(false);
        htmlPanelGrid1.getChildren().add(normalizerNameCreateInput);
        
        Message normalizerNameCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        normalizerNameCreateInputMessage.setId("normalizerNameCreateInputMessage");
        normalizerNameCreateInputMessage.setFor("normalizerNameCreateInput");
        normalizerNameCreateInputMessage.setDisplay("icon");
        htmlPanelGrid1.getChildren().add(normalizerNameCreateInputMessage);
        
        OutputLabel normalizerTypeCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        normalizerTypeCreateOutput.setFor("normalizerTypeCreateInput");
        normalizerTypeCreateOutput.setId("normalizerTypeCreateOutput");
        normalizerTypeCreateOutput.setValue("Normalizer Type:");
        htmlPanelGrid1.getChildren().add(normalizerTypeCreateOutput);
        
        Spinner normalizerTypeCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        normalizerTypeCreateInput.setId("normalizerTypeCreateInput");
        normalizerTypeCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.normalizerType}", Integer.class));
        normalizerTypeCreateInput.setRequired(false);
        
        htmlPanelGrid1.getChildren().add(normalizerTypeCreateInput);
        
        Message normalizerTypeCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        normalizerTypeCreateInputMessage.setId("normalizerTypeCreateInputMessage");
        normalizerTypeCreateInputMessage.setFor("normalizerTypeCreateInput");
        normalizerTypeCreateInputMessage.setDisplay("icon");
        htmlPanelGrid1.getChildren().add(normalizerTypeCreateInputMessage);
        
        OutputLabel normalizerStateCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        normalizerStateCreateOutput.setFor("normalizerStateCreateInput");
        normalizerStateCreateOutput.setId("normalizerStateCreateOutput");
        normalizerStateCreateOutput.setValue("Normalizer State:");
        htmlPanelGrid1.getChildren().add(normalizerStateCreateOutput);
        
        Spinner normalizerStateCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        normalizerStateCreateInput.setId("normalizerStateCreateInput");
        normalizerStateCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.normalizerState}", Integer.class));
        normalizerStateCreateInput.setRequired(false);
        
        htmlPanelGrid1.getChildren().add(normalizerStateCreateInput);
        
        Message normalizerStateCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        normalizerStateCreateInputMessage.setId("normalizerStateCreateInputMessage");
        normalizerStateCreateInputMessage.setFor("normalizerStateCreateInput");
        normalizerStateCreateInputMessage.setDisplay("icon");
        htmlPanelGrid1.getChildren().add(normalizerStateCreateInputMessage);
        
        OutputLabel defaultValueCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        defaultValueCreateOutput.setFor("defaultValueCreateInput");
        defaultValueCreateOutput.setId("defaultValueCreateOutput");
        defaultValueCreateOutput.setValue("Default Value:");
        htmlPanelGrid1.getChildren().add(defaultValueCreateOutput);
        
        Spinner defaultValueCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        defaultValueCreateInput.setId("defaultValueCreateInput");
        defaultValueCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.defaultValue}", Integer.class));
        defaultValueCreateInput.setRequired(false);
        
        htmlPanelGrid1.getChildren().add(defaultValueCreateInput);
        
        Message defaultValueCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        defaultValueCreateInputMessage.setId("defaultValueCreateInputMessage");
        defaultValueCreateInputMessage.setFor("defaultValueCreateInput");
        defaultValueCreateInputMessage.setDisplay("icon");
        htmlPanelGrid1.getChildren().add(defaultValueCreateInputMessage);
        
        OutputLabel valueIfNullCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        valueIfNullCreateOutput.setFor("valueIfNullCreateInput");
        valueIfNullCreateOutput.setId("valueIfNullCreateOutput");
        valueIfNullCreateOutput.setValue("Value If Null:");
        htmlPanelGrid1.getChildren().add(valueIfNullCreateOutput);
        
        Spinner valueIfNullCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        valueIfNullCreateInput.setId("valueIfNullCreateInput");
        valueIfNullCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.valueIfNull}", Integer.class));
        valueIfNullCreateInput.setRequired(false);
        
        htmlPanelGrid1.getChildren().add(valueIfNullCreateInput);
        
        Message valueIfNullCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        valueIfNullCreateInputMessage.setId("valueIfNullCreateInputMessage");
        valueIfNullCreateInputMessage.setFor("valueIfNullCreateInput");
        valueIfNullCreateInputMessage.setDisplay("icon");
        htmlPanelGrid1.getChildren().add(valueIfNullCreateInputMessage);
        
        OutputLabel inputFieldCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        inputFieldCreateOutput.setFor("inputFieldCreateInput");
        inputFieldCreateOutput.setId("inputFieldCreateOutput");
        inputFieldCreateOutput.setValue("Input Field:");
        htmlPanelGrid1.getChildren().add(inputFieldCreateOutput);
        
        InputText inputFieldCreateInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        inputFieldCreateInput.setId("inputFieldCreateInput");
        inputFieldCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.inputField}", String.class));
        inputFieldCreateInput.setRequired(false);
        htmlPanelGrid1.getChildren().add(inputFieldCreateInput);
        
        Message inputFieldCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        inputFieldCreateInputMessage.setId("inputFieldCreateInputMessage");
        inputFieldCreateInputMessage.setFor("inputFieldCreateInput");
        inputFieldCreateInputMessage.setDisplay("icon");
        htmlPanelGrid1.getChildren().add(inputFieldCreateInputMessage);
        
        OutputLabel specialFieldsCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        specialFieldsCreateOutput.setFor("specialFieldsCreateInput");
        specialFieldsCreateOutput.setId("specialFieldsCreateOutput");
        specialFieldsCreateOutput.setValue("Special Fields:");
        htmlPanelGrid2.getChildren().add(specialFieldsCreateOutput);
        
        InputText specialFieldsCreateInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        specialFieldsCreateInput.setId("specialFieldsCreateInput");
        specialFieldsCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.specialFields}", String.class));
        specialFieldsCreateInput.setRequired(false);
        htmlPanelGrid2.getChildren().add(specialFieldsCreateInput);
        
        Message specialFieldsCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        specialFieldsCreateInputMessage.setId("specialFieldsCreateInputMessage");
        specialFieldsCreateInputMessage.setFor("specialFieldsCreateInput");
        specialFieldsCreateInputMessage.setDisplay("icon");
        htmlPanelGrid2.getChildren().add(specialFieldsCreateInputMessage);
        
        OutputLabel categoryIdCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryIdCreateOutput.setFor("categoryIdCreateInput");
        categoryIdCreateOutput.setId("categoryIdCreateOutput");
        categoryIdCreateOutput.setValue("Category Id:");
        htmlPanelGrid2.getChildren().add(categoryIdCreateOutput);
        
        AutoComplete categoryIdCreateInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        categoryIdCreateInput.setId("categoryIdCreateInput");
        categoryIdCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.categoryId}", Category.class));
        categoryIdCreateInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{normalizerBean.completeCategoryId}", List.class, new Class[] { String.class }));
        categoryIdCreateInput.setDropdown(true);
        categoryIdCreateInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "categoryId", String.class));
        categoryIdCreateInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{categoryId.categoryType} #{categoryId.categoryName} #{categoryId.categoryParentId} #{categoryId.categoryId}", String.class));
        categoryIdCreateInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{categoryId}", Category.class));
        categoryIdCreateInput.setConverter(new CategoryConverter());
        categoryIdCreateInput.setRequired(false);
        htmlPanelGrid2.getChildren().add(categoryIdCreateInput);
        
        Message categoryIdCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryIdCreateInputMessage.setId("categoryIdCreateInputMessage");
        categoryIdCreateInputMessage.setFor("categoryIdCreateInput");
        categoryIdCreateInputMessage.setDisplay("icon");
        htmlPanelGrid2.getChildren().add(categoryIdCreateInputMessage);
        
        OutputLabel queryStateCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        queryStateCreateOutput.setFor("queryStateCreateInput");
        queryStateCreateOutput.setId("queryStateCreateOutput");
        queryStateCreateOutput.setValue("Query State:");
        htmlPanelGrid2.getChildren().add(queryStateCreateOutput);
        
        Spinner queryStateCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        queryStateCreateInput.setId("queryStateCreateInput");
        queryStateCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.queryState}", Integer.class));
        queryStateCreateInput.setRequired(false);
        
        htmlPanelGrid2.getChildren().add(queryStateCreateInput);
        
        Message queryStateCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        queryStateCreateInputMessage.setId("queryStateCreateInputMessage");
        queryStateCreateInputMessage.setFor("queryStateCreateInput");
        queryStateCreateInputMessage.setDisplay("icon");
        htmlPanelGrid2.getChildren().add(queryStateCreateInputMessage);
        
        OutputLabel effDateCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        effDateCreateOutput.setFor("effDateCreateInput");
        effDateCreateOutput.setId("effDateCreateOutput");
        effDateCreateOutput.setValue("Eff Date:");
        htmlPanelGrid2.getChildren().add(effDateCreateOutput);
        
        Calendar effDateCreateInput = (Calendar) application.createComponent(Calendar.COMPONENT_TYPE);
        effDateCreateInput.setId("effDateCreateInput");
        effDateCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.effDate}", Date.class));
        effDateCreateInput.setNavigator(true);
        effDateCreateInput.setEffect("slideDown");
        effDateCreateInput.setPattern("dd/MM/yyyy");
        effDateCreateInput.setRequired(false);
        htmlPanelGrid2.getChildren().add(effDateCreateInput);
        
        Message effDateCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        effDateCreateInputMessage.setId("effDateCreateInputMessage");
        effDateCreateInputMessage.setFor("effDateCreateInput");
        effDateCreateInputMessage.setDisplay("icon");
        htmlPanelGrid2.getChildren().add(effDateCreateInputMessage);
        
        OutputLabel expDateCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        expDateCreateOutput.setFor("expDateCreateInput");
        expDateCreateOutput.setId("expDateCreateOutput");
        expDateCreateOutput.setValue("Exp Date:");
        htmlPanelGrid2.getChildren().add(expDateCreateOutput);
        
        Calendar expDateCreateInput = (Calendar) application.createComponent(Calendar.COMPONENT_TYPE);
        expDateCreateInput.setId("expDateCreateInput");
        expDateCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.expDate}", Date.class));
        expDateCreateInput.setNavigator(true);
        expDateCreateInput.setEffect("slideDown");
        expDateCreateInput.setPattern("dd/MM/yyyy");
        expDateCreateInput.setRequired(false);
        htmlPanelGrid2.getChildren().add(expDateCreateInput);
        
        Message expDateCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        expDateCreateInputMessage.setId("expDateCreateInputMessage");
        expDateCreateInputMessage.setFor("expDateCreateInput");
        expDateCreateInputMessage.setDisplay("icon");
        htmlPanelGrid2.getChildren().add(expDateCreateInputMessage);
        
        OutputLabel descriptionCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        descriptionCreateOutput.setFor("descriptionCreateInput");
        descriptionCreateOutput.setId("descriptionCreateOutput");
        descriptionCreateOutput.setValue("Description:");
        htmlPanelGrid2.getChildren().add(descriptionCreateOutput);
        
        InputText descriptionCreateInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        descriptionCreateInput.setId("descriptionCreateInput");
        descriptionCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{normalizerBean.normalizer.description}", String.class));
        descriptionCreateInput.setRequired(false);
        htmlPanelGrid2.getChildren().add(descriptionCreateInput);
        
        Message descriptionCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        descriptionCreateInputMessage.setId("descriptionCreateInputMessage");
        descriptionCreateInputMessage.setFor("descriptionCreateInput");
        descriptionCreateInputMessage.setDisplay("icon");
        htmlPanelGrid2.getChildren().add(descriptionCreateInputMessage);
        
        
        
        
        return htmlPanelGrid;
    }
}
