package com.nw.web;
import com.nw.domain.ThresholdBalTypeMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ThresholdBalTypeMap.class, beanName = "thresholdBalTypeMapBean")
public class ThresholdBalTypeMapBean {
}
