package com.nw.web;
import com.nw.domain.Zones;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Zones.class, beanName = "zonesBean")
public class ZonesBean {
}
