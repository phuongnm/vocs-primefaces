package com.nw.web;
import com.nw.domain.DecisionTable;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = DecisionTable.class, beanName = "decisionTableBean")
public class DecisionTableBean {
}
