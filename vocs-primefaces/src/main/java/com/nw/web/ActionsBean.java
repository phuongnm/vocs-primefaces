package com.nw.web;
import com.nw.domain.Actions;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Actions.class, beanName = "actionsBean")
public class ActionsBean {
}
