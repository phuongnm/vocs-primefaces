package com.nw.web;
import com.nw.domain.CalcUnit;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = CalcUnit.class, beanName = "calcUnitBean")
public class CalcUnitBean {
}
