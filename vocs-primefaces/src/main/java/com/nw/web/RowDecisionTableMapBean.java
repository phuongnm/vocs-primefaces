package com.nw.web;
import com.nw.domain.RowDecisionTableMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = RowDecisionTableMap.class, beanName = "rowDecisionTableMapBean")
public class RowDecisionTableMapBean {
}
