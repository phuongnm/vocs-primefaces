package com.nw.web;
import com.nw.domain.BlockRateTableMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = BlockRateTableMap.class, beanName = "blockRateTableMapBean")
public class BlockRateTableMapBean {
}
