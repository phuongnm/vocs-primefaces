package com.nw.web;
import com.nw.domain.DynamicReserveRateTableMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = DynamicReserveRateTableMap.class, beanName = "dynamicReserveRateTableMapBean")
public class DynamicReserveRateTableMapBean {
}
