package com.nw.web;
import com.nw.domain.GeoHomeZone;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = GeoHomeZone.class, beanName = "geoHomeZoneBean")
public class GeoHomeZoneBean {
}
