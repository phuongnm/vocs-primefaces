package com.nw.web;
import com.nw.domain.TriggerFieldList;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerFieldList.class, beanName = "triggerFieldListBean")
public class TriggerFieldListBean {
}
