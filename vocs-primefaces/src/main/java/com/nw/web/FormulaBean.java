package com.nw.web;
import com.nw.domain.Formula;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Formula.class, beanName = "formulaBean")
public class FormulaBean {
}
