package com.nw.web.tree.target;

import com.nw.domain.Category;

public class CategoryNestedSetTreeNodeTarget implements NestedSetTreeNodeTarget {

	@Override
	public String getNodeName(long targetId) {
		Category category = Category.findCategory(targetId);
		if(category != null) {
			return category.getCategoryName();
		}
		
		return null;
	}

}
