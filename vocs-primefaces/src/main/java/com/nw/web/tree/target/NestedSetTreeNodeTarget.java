package com.nw.web.tree.target;

public interface NestedSetTreeNodeTarget {
	
	public String getNodeName(long targetId);
	
}
