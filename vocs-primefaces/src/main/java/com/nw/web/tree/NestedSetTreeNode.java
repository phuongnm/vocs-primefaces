package com.nw.web.tree;

import java.util.List;

import com.nw.web.tree.target.NestedSetTreeNodeTarget;
import com.nw.web.tree.target.TargetName;

public abstract class NestedSetTreeNode {
	
	public static NestedSetTreeNode getInstance(TreeType treeType) {
		try {
			NestedSetTreeNode node = (NestedSetTreeNode) treeType.getClassInstance().getConstructors()[0].newInstance();
			node.setTreeType(treeType);
			
			return node;
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public static String getNodeName(String value, long targetId) {
		
		try {
			TargetName temp = TargetName.getByValue(value);
			NestedSetTreeNodeTarget target = (NestedSetTreeNodeTarget) temp.getClassInstance().getConstructors()[0].newInstance();
			
			return target.getNodeName(targetId);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;
	}

	public  abstract NestedSetTreeNode getRootTreeNode();
	public abstract List<NestedSetTreeNode> getChildrenTreeNodes();
	
	private long id;
	private String text;
	private NodeType nodeType;
	private TreeType treeType;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

	public TreeType getTreeType() {
		return treeType;
	}

	public void setTreeType(TreeType treeType) {
		this.treeType = treeType;
	}	
}
