package com.nw.web.tree;

import javassist.NotFoundException;

public enum TreeType {

	NORMALIZER(22, NormalizerNestedSetTreeNode.class);
	
	private int value;
	private Class classInstance;
	
	private TreeType(int value, Class classInstance) {
		this.value = value;
		this.classInstance = classInstance;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public Class getClassInstance() {
		return this.classInstance;
	}
	
	public TreeType getByValue(int value) throws NotFoundException {
		for (TreeType treeType : TreeType.values()) {
			if (treeType.getValue() == value) {
				return treeType;
			}
		}
		throw new NotFoundException("No TreeType defined by " + value);
	}
}
