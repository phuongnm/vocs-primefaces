package com.nw.web.tree;

import java.util.ArrayList;
import java.util.List;

import javassist.NotFoundException;

import com.nw.domain.NestedSet;

public class NormalizerNestedSetTreeNode extends NestedSetTreeNode {

	@Override
	public NestedSetTreeNode getRootTreeNode() {
		NestedSet temp = NestedSet.findRootByTreeType(this.getTreeType().getValue());
		if(temp != null) {
			NestedSetTreeNode node = new NormalizerNestedSetTreeNode();
			node.setId(temp.getId());
			node.setNodeType(NodeType.NORMALIZER_ROOT);
			node.setText(temp.getNodeName());
			node.setTreeType(this.getTreeType());
			return node;
		}
		return null;
	}

	@Override
	public List<NestedSetTreeNode> getChildrenTreeNodes() {
		
		List<NestedSet> temps = NestedSet.findChildrenByTreeTypeAndId(this.getTreeType().getValue(), this.getId());
		if(temps != null && temps.size() > 0) {
			List<NestedSetTreeNode> nodes = new ArrayList<NestedSetTreeNode>();
			for(NestedSet temp : temps) {
				NestedSetTreeNode node = new NormalizerNestedSetTreeNode();
				node.setId(temp.getId());
				node.setText(NestedSetTreeNode.getNodeName(temp.getTargetName(), temp.getTargetId()));
				try {
					NodeType nodeType = NodeType.getByTreeTypeAndTargetName(this.getTreeType(), temp.getTargetName());
					node.setNodeType(nodeType);
				} catch (NotFoundException ex) {
					ex.printStackTrace();
				}
				node.setTreeType(this.getTreeType());
				nodes.add(node);
			}
			
			return nodes;
		}
		
		return null;
	}
	
}
