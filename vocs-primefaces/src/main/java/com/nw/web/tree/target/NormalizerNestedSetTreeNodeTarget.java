package com.nw.web.tree.target;

import com.nw.domain.Normalizer;

public class NormalizerNestedSetTreeNodeTarget implements NestedSetTreeNodeTarget {

	@Override
	public String getNodeName(long targetId) {
		Normalizer normalizer = Normalizer.findNormalizer(targetId);
		if(normalizer != null) {
			return normalizer.getNormalizerName();
		}
		
		return null;
	}
}
