package com.nw.web;
import com.nw.domain.TriggerType;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerType.class, beanName = "triggerTypeBean")
public class TriggerTypeBean {
}
