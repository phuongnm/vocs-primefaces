// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web;

import com.nw.domain.ZoneData;
import com.nw.domain.Zones;
import com.nw.web.ZoneDataBean;
import com.nw.web.converter.ZonesConverter;
import com.nw.web.util.MessageFactory;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.message.Message;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

privileged aspect ZoneDataBean_Roo_ManagedBean {
    
    declare @type: ZoneDataBean: @ManagedBean(name = "zoneDataBean");
    
    declare @type: ZoneDataBean: @SessionScoped;
    
    private String ZoneDataBean.name = "ZoneDatas";
    
    private ZoneData ZoneDataBean.zoneData;
    
    private List<ZoneData> ZoneDataBean.allZoneDatas;
    
    private boolean ZoneDataBean.dataVisible = false;
    
    private List<String> ZoneDataBean.columns;
    
    private HtmlPanelGrid ZoneDataBean.createPanelGrid;
    
    private HtmlPanelGrid ZoneDataBean.editPanelGrid;
    
    private HtmlPanelGrid ZoneDataBean.viewPanelGrid;
    
    private boolean ZoneDataBean.createDialogVisible = false;
    
    @PostConstruct
    public void ZoneDataBean.init() {
        columns = new ArrayList<String>();
        columns.add("zoneDataValue");
    }
    
    public String ZoneDataBean.getName() {
        return name;
    }
    
    public List<String> ZoneDataBean.getColumns() {
        return columns;
    }
    
    public List<ZoneData> ZoneDataBean.getAllZoneDatas() {
        return allZoneDatas;
    }
    
    public void ZoneDataBean.setAllZoneDatas(List<ZoneData> allZoneDatas) {
        this.allZoneDatas = allZoneDatas;
    }
    
    public String ZoneDataBean.findAllZoneDatas() {
        allZoneDatas = ZoneData.findAllZoneDatas();
        dataVisible = !allZoneDatas.isEmpty();
        return null;
    }
    
    public boolean ZoneDataBean.isDataVisible() {
        return dataVisible;
    }
    
    public void ZoneDataBean.setDataVisible(boolean dataVisible) {
        this.dataVisible = dataVisible;
    }
    
    public HtmlPanelGrid ZoneDataBean.getCreatePanelGrid() {
        if (createPanelGrid == null) {
            createPanelGrid = populateCreatePanel();
        }
        return createPanelGrid;
    }
    
    public void ZoneDataBean.setCreatePanelGrid(HtmlPanelGrid createPanelGrid) {
        this.createPanelGrid = createPanelGrid;
    }
    
    public HtmlPanelGrid ZoneDataBean.getEditPanelGrid() {
        if (editPanelGrid == null) {
            editPanelGrid = populateEditPanel();
        }
        return editPanelGrid;
    }
    
    public void ZoneDataBean.setEditPanelGrid(HtmlPanelGrid editPanelGrid) {
        this.editPanelGrid = editPanelGrid;
    }
    
    public HtmlPanelGrid ZoneDataBean.getViewPanelGrid() {
        return populateViewPanel();
    }
    
    public void ZoneDataBean.setViewPanelGrid(HtmlPanelGrid viewPanelGrid) {
        this.viewPanelGrid = viewPanelGrid;
    }
    
    public HtmlPanelGrid ZoneDataBean.populateCreatePanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel zoneDataValueCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        zoneDataValueCreateOutput.setFor("zoneDataValueCreateInput");
        zoneDataValueCreateOutput.setId("zoneDataValueCreateOutput");
        zoneDataValueCreateOutput.setValue("Zone Data Value:");
        htmlPanelGrid.getChildren().add(zoneDataValueCreateOutput);
        
        InputText zoneDataValueCreateInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        zoneDataValueCreateInput.setId("zoneDataValueCreateInput");
        zoneDataValueCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{zoneDataBean.zoneData.zoneDataValue}", String.class));
        zoneDataValueCreateInput.setRequired(false);
        htmlPanelGrid.getChildren().add(zoneDataValueCreateInput);
        
        Message zoneDataValueCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        zoneDataValueCreateInputMessage.setId("zoneDataValueCreateInputMessage");
        zoneDataValueCreateInputMessage.setFor("zoneDataValueCreateInput");
        zoneDataValueCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(zoneDataValueCreateInputMessage);
        
        OutputLabel zoneIdCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        zoneIdCreateOutput.setFor("zoneIdCreateInput");
        zoneIdCreateOutput.setId("zoneIdCreateOutput");
        zoneIdCreateOutput.setValue("Zone Id:");
        htmlPanelGrid.getChildren().add(zoneIdCreateOutput);
        
        AutoComplete zoneIdCreateInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        zoneIdCreateInput.setId("zoneIdCreateInput");
        zoneIdCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{zoneDataBean.zoneData.zoneId}", Zones.class));
        zoneIdCreateInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{zoneDataBean.completeZoneId}", List.class, new Class[] { String.class }));
        zoneIdCreateInput.setDropdown(true);
        zoneIdCreateInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "zoneId", String.class));
        zoneIdCreateInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{zoneId.zoneName} #{zoneId.zoneCode} #{zoneId.remark} #{zoneId.zoneId}", String.class));
        zoneIdCreateInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{zoneId}", Zones.class));
        zoneIdCreateInput.setConverter(new ZonesConverter());
        zoneIdCreateInput.setRequired(false);
        htmlPanelGrid.getChildren().add(zoneIdCreateInput);
        
        Message zoneIdCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        zoneIdCreateInputMessage.setId("zoneIdCreateInputMessage");
        zoneIdCreateInputMessage.setFor("zoneIdCreateInput");
        zoneIdCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(zoneIdCreateInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid ZoneDataBean.populateEditPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel zoneDataValueEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        zoneDataValueEditOutput.setFor("zoneDataValueEditInput");
        zoneDataValueEditOutput.setId("zoneDataValueEditOutput");
        zoneDataValueEditOutput.setValue("Zone Data Value:");
        htmlPanelGrid.getChildren().add(zoneDataValueEditOutput);
        
        InputText zoneDataValueEditInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        zoneDataValueEditInput.setId("zoneDataValueEditInput");
        zoneDataValueEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{zoneDataBean.zoneData.zoneDataValue}", String.class));
        zoneDataValueEditInput.setRequired(false);
        htmlPanelGrid.getChildren().add(zoneDataValueEditInput);
        
        Message zoneDataValueEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        zoneDataValueEditInputMessage.setId("zoneDataValueEditInputMessage");
        zoneDataValueEditInputMessage.setFor("zoneDataValueEditInput");
        zoneDataValueEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(zoneDataValueEditInputMessage);
        
        OutputLabel zoneIdEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        zoneIdEditOutput.setFor("zoneIdEditInput");
        zoneIdEditOutput.setId("zoneIdEditOutput");
        zoneIdEditOutput.setValue("Zone Id:");
        htmlPanelGrid.getChildren().add(zoneIdEditOutput);
        
        AutoComplete zoneIdEditInput = (AutoComplete) application.createComponent(AutoComplete.COMPONENT_TYPE);
        zoneIdEditInput.setId("zoneIdEditInput");
        zoneIdEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{zoneDataBean.zoneData.zoneId}", Zones.class));
        zoneIdEditInput.setCompleteMethod(expressionFactory.createMethodExpression(elContext, "#{zoneDataBean.completeZoneId}", List.class, new Class[] { String.class }));
        zoneIdEditInput.setDropdown(true);
        zoneIdEditInput.setValueExpression("var", expressionFactory.createValueExpression(elContext, "zoneId", String.class));
        zoneIdEditInput.setValueExpression("itemLabel", expressionFactory.createValueExpression(elContext, "#{zoneId.zoneName} #{zoneId.zoneCode} #{zoneId.remark} #{zoneId.zoneId}", String.class));
        zoneIdEditInput.setValueExpression("itemValue", expressionFactory.createValueExpression(elContext, "#{zoneId}", Zones.class));
        zoneIdEditInput.setConverter(new ZonesConverter());
        zoneIdEditInput.setRequired(false);
        htmlPanelGrid.getChildren().add(zoneIdEditInput);
        
        Message zoneIdEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        zoneIdEditInputMessage.setId("zoneIdEditInputMessage");
        zoneIdEditInputMessage.setFor("zoneIdEditInput");
        zoneIdEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(zoneIdEditInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid ZoneDataBean.populateViewPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        HtmlOutputText zoneDataValueLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        zoneDataValueLabel.setId("zoneDataValueLabel");
        zoneDataValueLabel.setValue("Zone Data Value:");
        htmlPanelGrid.getChildren().add(zoneDataValueLabel);
        
        HtmlOutputText zoneDataValueValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        zoneDataValueValue.setId("zoneDataValueValue");
        zoneDataValueValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{zoneDataBean.zoneData.zoneDataValue}", String.class));
        htmlPanelGrid.getChildren().add(zoneDataValueValue);
        
        HtmlOutputText zoneIdLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        zoneIdLabel.setId("zoneIdLabel");
        zoneIdLabel.setValue("Zone Id:");
        htmlPanelGrid.getChildren().add(zoneIdLabel);
        
        HtmlOutputText zoneIdValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        zoneIdValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{zoneDataBean.zoneData.zoneId}", Zones.class));
        zoneIdValue.setConverter(new ZonesConverter());
        htmlPanelGrid.getChildren().add(zoneIdValue);
        
        return htmlPanelGrid;
    }
    
    public ZoneData ZoneDataBean.getZoneData() {
        if (zoneData == null) {
            zoneData = new ZoneData();
        }
        return zoneData;
    }
    
    public void ZoneDataBean.setZoneData(ZoneData zoneData) {
        this.zoneData = zoneData;
    }
    
    public List<Zones> ZoneDataBean.completeZoneId(String query) {
        List<Zones> suggestions = new ArrayList<Zones>();
        for (Zones zones : Zones.findAllZoneses()) {
            String zonesStr = String.valueOf(zones.getZoneName() +  " "  + zones.getZoneCode() +  " "  + zones.getRemark() +  " "  + zones.getZoneId());
            if (zonesStr.toLowerCase().startsWith(query.toLowerCase())) {
                suggestions.add(zones);
            }
        }
        return suggestions;
    }
    
    public String ZoneDataBean.onEdit() {
        return null;
    }
    
    public boolean ZoneDataBean.isCreateDialogVisible() {
        return createDialogVisible;
    }
    
    public void ZoneDataBean.setCreateDialogVisible(boolean createDialogVisible) {
        this.createDialogVisible = createDialogVisible;
    }
    
    public String ZoneDataBean.displayList() {
        createDialogVisible = false;
        findAllZoneDatas();
        return "zoneData";
    }
    
    public String ZoneDataBean.displayCreateDialog() {
        zoneData = new ZoneData();
        createDialogVisible = true;
        return "zoneData";
    }
    
    public String ZoneDataBean.persist() {
        String message = "";
        if (zoneData.getZoneDataId() != null) {
            zoneData.merge();
            message = "message_successfully_updated";
        } else {
            zoneData.persist();
            message = "message_successfully_created";
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("createDialogWidget.hide()");
        context.execute("editDialogWidget.hide()");
        
        FacesMessage facesMessage = MessageFactory.getMessage(message, "ZoneData");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllZoneDatas();
    }
    
    public String ZoneDataBean.delete() {
        zoneData.remove();
        FacesMessage facesMessage = MessageFactory.getMessage("message_successfully_deleted", "ZoneData");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllZoneDatas();
    }
    
    public void ZoneDataBean.reset() {
        zoneData = null;
        createDialogVisible = false;
    }
    
    public void ZoneDataBean.handleDialogClose(CloseEvent event) {
        reset();
    }
    
}
