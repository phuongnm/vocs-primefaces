package com.nw.web;
import com.nw.domain.TriggerMsgField;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerMsgField.class, beanName = "triggerMsgFieldBean")
public class TriggerMsgFieldBean {
}
