package com.nw.web;
import com.nw.domain.NormalizerNormValueMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = NormalizerNormValueMap.class, beanName = "normalizerNormValueMapBean")
public class NormalizerNormValueMapBean {
}
