package com.nw.web;
import com.nw.domain.BillingCycleType;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = BillingCycleType.class, beanName = "billingCycleTypeBean")
public class BillingCycleTypeBean {
}
