package com.nw.web;
import com.nw.domain.ColumnDecisionTableMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ColumnDecisionTableMap.class, beanName = "columnDecisionTableMapBean")
public class ColumnDecisionTableMapBean {
}
