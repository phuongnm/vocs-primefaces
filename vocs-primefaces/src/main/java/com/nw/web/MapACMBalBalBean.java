package com.nw.web;
import com.nw.domain.MapACMBalBal;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = MapACMBalBal.class, beanName = "mapACMBalBalBean")
public class MapACMBalBalBean {
}
