package com.nw.web;
import com.nw.domain.TriggerFieldBuilder;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerFieldBuilder.class, beanName = "triggerFieldBuilderBean")
public class TriggerFieldBuilderBean {
}
