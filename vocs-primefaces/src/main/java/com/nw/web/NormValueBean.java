package com.nw.web;
import com.nw.domain.NormValue;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = NormValue.class, beanName = "normValueBean")
public class NormValueBean {
}
