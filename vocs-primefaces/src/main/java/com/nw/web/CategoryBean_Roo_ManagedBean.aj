// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web;

import com.nw.domain.Category;
import com.nw.web.CategoryBean;
import com.nw.web.util.MessageFactory;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.message.Message;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.spinner.Spinner;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

privileged aspect CategoryBean_Roo_ManagedBean {
    
    declare @type: CategoryBean: @ManagedBean(name = "categoryBean");
    
    declare @type: CategoryBean: @SessionScoped;
    
    private String CategoryBean.name = "Categorys";
    
    private Category CategoryBean.category;
    
    private List<Category> CategoryBean.allCategorys;
    
    private boolean CategoryBean.dataVisible = false;
    
    private List<String> CategoryBean.columns;
    
    private HtmlPanelGrid CategoryBean.createPanelGrid;
    
    private HtmlPanelGrid CategoryBean.editPanelGrid;
    
    private HtmlPanelGrid CategoryBean.viewPanelGrid;
    
    private boolean CategoryBean.createDialogVisible = false;
    
    @PostConstruct
    public void CategoryBean.init() {
        columns = new ArrayList<String>();
        columns.add("categoryType");
        columns.add("categoryName");
        columns.add("categoryParentId");
    }
    
    public String CategoryBean.getName() {
        return name;
    }
    
    public List<String> CategoryBean.getColumns() {
        return columns;
    }
    
    public List<Category> CategoryBean.getAllCategorys() {
        return allCategorys;
    }
    
    public void CategoryBean.setAllCategorys(List<Category> allCategorys) {
        this.allCategorys = allCategorys;
    }
    
    public String CategoryBean.findAllCategorys() {
        allCategorys = Category.findAllCategorys();
        dataVisible = !allCategorys.isEmpty();
        return null;
    }
    
    public boolean CategoryBean.isDataVisible() {
        return dataVisible;
    }
    
    public void CategoryBean.setDataVisible(boolean dataVisible) {
        this.dataVisible = dataVisible;
    }
    
    public HtmlPanelGrid CategoryBean.getCreatePanelGrid() {
        if (createPanelGrid == null) {
            createPanelGrid = populateCreatePanel();
        }
        return createPanelGrid;
    }
    
    public void CategoryBean.setCreatePanelGrid(HtmlPanelGrid createPanelGrid) {
        this.createPanelGrid = createPanelGrid;
    }
    
    public HtmlPanelGrid CategoryBean.getEditPanelGrid() {
        if (editPanelGrid == null) {
            editPanelGrid = populateEditPanel();
        }
        return editPanelGrid;
    }
    
    public void CategoryBean.setEditPanelGrid(HtmlPanelGrid editPanelGrid) {
        this.editPanelGrid = editPanelGrid;
    }
    
    public HtmlPanelGrid CategoryBean.getViewPanelGrid() {
        return populateViewPanel();
    }
    
    public void CategoryBean.setViewPanelGrid(HtmlPanelGrid viewPanelGrid) {
        this.viewPanelGrid = viewPanelGrid;
    }
    
    public HtmlPanelGrid CategoryBean.populateCreatePanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel categoryTypeCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryTypeCreateOutput.setFor("categoryTypeCreateInput");
        categoryTypeCreateOutput.setId("categoryTypeCreateOutput");
        categoryTypeCreateOutput.setValue("Category Type:");
        htmlPanelGrid.getChildren().add(categoryTypeCreateOutput);
        
        Spinner categoryTypeCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        categoryTypeCreateInput.setId("categoryTypeCreateInput");
        categoryTypeCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryType}", Integer.class));
        categoryTypeCreateInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(categoryTypeCreateInput);
        
        Message categoryTypeCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryTypeCreateInputMessage.setId("categoryTypeCreateInputMessage");
        categoryTypeCreateInputMessage.setFor("categoryTypeCreateInput");
        categoryTypeCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(categoryTypeCreateInputMessage);
        
        OutputLabel categoryNameCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryNameCreateOutput.setFor("categoryNameCreateInput");
        categoryNameCreateOutput.setId("categoryNameCreateOutput");
        categoryNameCreateOutput.setValue("Category Name:");
        htmlPanelGrid.getChildren().add(categoryNameCreateOutput);
        
        InputText categoryNameCreateInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        categoryNameCreateInput.setId("categoryNameCreateInput");
        categoryNameCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryName}", String.class));
        categoryNameCreateInput.setRequired(false);
        htmlPanelGrid.getChildren().add(categoryNameCreateInput);
        
        Message categoryNameCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryNameCreateInputMessage.setId("categoryNameCreateInputMessage");
        categoryNameCreateInputMessage.setFor("categoryNameCreateInput");
        categoryNameCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(categoryNameCreateInputMessage);
        
        OutputLabel categoryParentIdCreateOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryParentIdCreateOutput.setFor("categoryParentIdCreateInput");
        categoryParentIdCreateOutput.setId("categoryParentIdCreateOutput");
        categoryParentIdCreateOutput.setValue("Category Parent Id:");
        htmlPanelGrid.getChildren().add(categoryParentIdCreateOutput);
        
        Spinner categoryParentIdCreateInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        categoryParentIdCreateInput.setId("categoryParentIdCreateInput");
        categoryParentIdCreateInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryParentId}", Integer.class));
        categoryParentIdCreateInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(categoryParentIdCreateInput);
        
        Message categoryParentIdCreateInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryParentIdCreateInputMessage.setId("categoryParentIdCreateInputMessage");
        categoryParentIdCreateInputMessage.setFor("categoryParentIdCreateInput");
        categoryParentIdCreateInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(categoryParentIdCreateInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid CategoryBean.populateEditPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        OutputLabel categoryTypeEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryTypeEditOutput.setFor("categoryTypeEditInput");
        categoryTypeEditOutput.setId("categoryTypeEditOutput");
        categoryTypeEditOutput.setValue("Category Type:");
        htmlPanelGrid.getChildren().add(categoryTypeEditOutput);
        
        Spinner categoryTypeEditInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        categoryTypeEditInput.setId("categoryTypeEditInput");
        categoryTypeEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryType}", Integer.class));
        categoryTypeEditInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(categoryTypeEditInput);
        
        Message categoryTypeEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryTypeEditInputMessage.setId("categoryTypeEditInputMessage");
        categoryTypeEditInputMessage.setFor("categoryTypeEditInput");
        categoryTypeEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(categoryTypeEditInputMessage);
        
        OutputLabel categoryNameEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryNameEditOutput.setFor("categoryNameEditInput");
        categoryNameEditOutput.setId("categoryNameEditOutput");
        categoryNameEditOutput.setValue("Category Name:");
        htmlPanelGrid.getChildren().add(categoryNameEditOutput);
        
        InputText categoryNameEditInput = (InputText) application.createComponent(InputText.COMPONENT_TYPE);
        categoryNameEditInput.setId("categoryNameEditInput");
        categoryNameEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryName}", String.class));
        categoryNameEditInput.setRequired(false);
        htmlPanelGrid.getChildren().add(categoryNameEditInput);
        
        Message categoryNameEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryNameEditInputMessage.setId("categoryNameEditInputMessage");
        categoryNameEditInputMessage.setFor("categoryNameEditInput");
        categoryNameEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(categoryNameEditInputMessage);
        
        OutputLabel categoryParentIdEditOutput = (OutputLabel) application.createComponent(OutputLabel.COMPONENT_TYPE);
        categoryParentIdEditOutput.setFor("categoryParentIdEditInput");
        categoryParentIdEditOutput.setId("categoryParentIdEditOutput");
        categoryParentIdEditOutput.setValue("Category Parent Id:");
        htmlPanelGrid.getChildren().add(categoryParentIdEditOutput);
        
        Spinner categoryParentIdEditInput = (Spinner) application.createComponent(Spinner.COMPONENT_TYPE);
        categoryParentIdEditInput.setId("categoryParentIdEditInput");
        categoryParentIdEditInput.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryParentId}", Integer.class));
        categoryParentIdEditInput.setRequired(false);
        
        htmlPanelGrid.getChildren().add(categoryParentIdEditInput);
        
        Message categoryParentIdEditInputMessage = (Message) application.createComponent(Message.COMPONENT_TYPE);
        categoryParentIdEditInputMessage.setId("categoryParentIdEditInputMessage");
        categoryParentIdEditInputMessage.setFor("categoryParentIdEditInput");
        categoryParentIdEditInputMessage.setDisplay("icon");
        htmlPanelGrid.getChildren().add(categoryParentIdEditInputMessage);
        
        return htmlPanelGrid;
    }
    
    public HtmlPanelGrid CategoryBean.populateViewPanel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        javax.faces.application.Application application = facesContext.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        
        HtmlPanelGrid htmlPanelGrid = (HtmlPanelGrid) application.createComponent(HtmlPanelGrid.COMPONENT_TYPE);
        
        HtmlOutputText categoryTypeLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        categoryTypeLabel.setId("categoryTypeLabel");
        categoryTypeLabel.setValue("Category Type:");
        htmlPanelGrid.getChildren().add(categoryTypeLabel);
        
        HtmlOutputText categoryTypeValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        categoryTypeValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryType}", String.class));
        htmlPanelGrid.getChildren().add(categoryTypeValue);
        
        HtmlOutputText categoryNameLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        categoryNameLabel.setId("categoryNameLabel");
        categoryNameLabel.setValue("Category Name:");
        htmlPanelGrid.getChildren().add(categoryNameLabel);
        
        HtmlOutputText categoryNameValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        categoryNameValue.setId("categoryNameValue");
        categoryNameValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryName}", String.class));
        htmlPanelGrid.getChildren().add(categoryNameValue);
        
        HtmlOutputText categoryParentIdLabel = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        categoryParentIdLabel.setId("categoryParentIdLabel");
        categoryParentIdLabel.setValue("Category Parent Id:");
        htmlPanelGrid.getChildren().add(categoryParentIdLabel);
        
        HtmlOutputText categoryParentIdValue = (HtmlOutputText) application.createComponent(HtmlOutputText.COMPONENT_TYPE);
        categoryParentIdValue.setValueExpression("value", expressionFactory.createValueExpression(elContext, "#{categoryBean.category.categoryParentId}", String.class));
        htmlPanelGrid.getChildren().add(categoryParentIdValue);
        
        return htmlPanelGrid;
    }
    
    public Category CategoryBean.getCategory() {
        if (category == null) {
            category = new Category();
        }
        return category;
    }
    
    public void CategoryBean.setCategory(Category category) {
        this.category = category;
    }
    
    public String CategoryBean.onEdit() {
        return null;
    }
    
    public boolean CategoryBean.isCreateDialogVisible() {
        return createDialogVisible;
    }
    
    public void CategoryBean.setCreateDialogVisible(boolean createDialogVisible) {
        this.createDialogVisible = createDialogVisible;
    }
    
    public String CategoryBean.displayList() {
        createDialogVisible = false;
        findAllCategorys();
        return "category";
    }
    
    public String CategoryBean.displayCreateDialog() {
        category = new Category();
        createDialogVisible = true;
        return "category";
    }
    
    public String CategoryBean.persist() {
        String message = "";
        if (category.getCategoryId() != null) {
            category.merge();
            message = "message_successfully_updated";
        } else {
            category.persist();
            message = "message_successfully_created";
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("createDialogWidget.hide()");
        context.execute("editDialogWidget.hide()");
        
        FacesMessage facesMessage = MessageFactory.getMessage(message, "Category");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllCategorys();
    }
    
    public String CategoryBean.delete() {
        category.remove();
        FacesMessage facesMessage = MessageFactory.getMessage("message_successfully_deleted", "Category");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return findAllCategorys();
    }
    
    public void CategoryBean.reset() {
        category = null;
        createDialogVisible = false;
    }
    
    public void CategoryBean.handleDialogClose(CloseEvent event) {
        reset();
    }
    
}
