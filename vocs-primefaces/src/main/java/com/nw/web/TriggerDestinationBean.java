package com.nw.web;
import com.nw.domain.TriggerDestination;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerDestination.class, beanName = "triggerDestinationBean")
public class TriggerDestinationBean {
}
