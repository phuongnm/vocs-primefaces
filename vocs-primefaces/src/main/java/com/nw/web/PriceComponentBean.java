package com.nw.web;
import com.nw.domain.PriceComponent;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = PriceComponent.class, beanName = "priceComponentBean")
public class PriceComponentBean {
}
