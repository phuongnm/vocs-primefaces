package com.nw.web;
import com.nw.domain.DynamicReserve;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = DynamicReserve.class, beanName = "dynamicReserveBean")
public class DynamicReserveBean {
}
