package com.nw.web;
import com.nw.domain.Offer;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Offer.class, beanName = "offerBean")
public class OfferBean {
}
