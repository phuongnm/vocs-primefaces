package com.nw.web;
import com.nw.domain.ZoneMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ZoneMap.class, beanName = "zoneMapBean")
public class ZoneMapBean {
}
