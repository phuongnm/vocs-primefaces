package com.nw.web;
import com.nw.domain.RateTable;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = RateTable.class, beanName = "rateTableBean")
public class RateTableBean {
}
