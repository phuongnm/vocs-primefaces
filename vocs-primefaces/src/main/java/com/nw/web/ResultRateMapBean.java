package com.nw.web;
import com.nw.domain.ResultRateMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ResultRateMap.class, beanName = "resultRateMapBean")
public class ResultRateMapBean {
}
