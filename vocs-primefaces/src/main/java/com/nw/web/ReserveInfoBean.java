package com.nw.web;
import com.nw.domain.ReserveInfo;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ReserveInfo.class, beanName = "reserveInfoBean")
public class ReserveInfoBean {
}
