package com.nw.web;
import com.nw.domain.PriceComponentBlockMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = PriceComponentBlockMap.class, beanName = "priceComponentBlockMapBean")
public class PriceComponentBlockMapBean {
}
