package com.nw.web;
import com.nw.domain.OfferParameterMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = OfferParameterMap.class, beanName = "offerParameterMapBean")
public class OfferParameterMapBean {
}
