package com.nw.web;
import java.util.List;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.nw.domain.NestedSet;
import com.nw.web.tree.NestedSetTreeNode;
import com.nw.web.tree.TreeType;

@RooSerializable
@RooJsfManagedBean(entity = NestedSet.class, beanName = "nestedSetBean")
public class NestedSetBean {
	
	public NestedSetBean() {
		
		initNormalizerTree();
	}
	
	public void onNodeSelect(NodeSelectEvent event){
		// TODO
	}

	public void onNodeExpand(NodeExpandEvent event){
		List<TreeNode> children = event.getTreeNode().getChildren();
		if(children != null && children.size() > 0) {
			for(TreeNode child : children) {
				NestedSetTreeNode nestedSetTreeNode = (NestedSetTreeNode) child.getData();
				List<NestedSetTreeNode> temps = nestedSetTreeNode.getChildrenTreeNodes();
				if(temps != null && temps.size() > 0) {
					for(NestedSetTreeNode temp : temps) {
						new DefaultTreeNode(temp.getNodeType().getValue(), temp, child);
					}
				}
			}
		}
	}
	
	private void initNormalizerTree() {
		this.normalizerRoot = new DefaultTreeNode("Root Node", null);
		
		NestedSetTreeNode normalizer = NestedSetTreeNode.getInstance(TreeType.NORMALIZER).getRootTreeNode();
		
		TreeNode root = new DefaultTreeNode(normalizer.getNodeType().getValue(), normalizer, this.normalizerRoot);
		
		List<NestedSetTreeNode> children = normalizer.getChildrenTreeNodes();
		if(children != null && children.size() > 0) {
			for(NestedSetTreeNode chil : children) {
				new DefaultTreeNode(chil.getNodeType().getValue(), chil, root);
			}
		}
	}
	
	private TreeNode normalizerRoot;
	private TreeNode selectedNode;

	public TreeNode getNormalizerRoot() {
		return normalizerRoot;
	}

	public void setNormalizerRoot(TreeNode normalizerRoot) {
		this.normalizerRoot = normalizerRoot;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
}
