package com.nw.web;
import com.nw.domain.ActionPriceComponentMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ActionPriceComponentMap.class, beanName = "actionPriceComponentMapBean")
public class ActionPriceComponentMapBean {
}
