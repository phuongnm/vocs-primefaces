package com.nw.web;
import com.nw.domain.TriggerEvent;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerEvent.class, beanName = "triggerEventBean")
public class TriggerEventBean {
}
