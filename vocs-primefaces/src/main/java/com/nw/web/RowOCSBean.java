package com.nw.web;
import com.nw.domain.RowOCS;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = RowOCS.class, beanName = "rowOCSBean")
public class RowOCSBean {
}
