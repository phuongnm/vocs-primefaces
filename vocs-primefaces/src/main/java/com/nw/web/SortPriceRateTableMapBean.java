package com.nw.web;
import com.nw.domain.SortPriceRateTableMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = SortPriceRateTableMap.class, beanName = "sortPriceRateTableMapBean")
public class SortPriceRateTableMapBean {
}
