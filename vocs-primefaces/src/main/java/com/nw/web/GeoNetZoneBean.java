package com.nw.web;
import com.nw.domain.GeoNetZone;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = GeoNetZone.class, beanName = "geoNetZoneBean")
public class GeoNetZoneBean {
}
