package com.nw.web.converter;
import com.nw.domain.GeoNetZone;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = GeoNetZone.class)
public class GeoNetZoneConverter implements Converter {
}
