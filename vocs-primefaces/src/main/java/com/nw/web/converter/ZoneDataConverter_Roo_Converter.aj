// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web.converter;

import com.nw.domain.ZoneData;
import com.nw.web.converter.ZoneDataConverter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

privileged aspect ZoneDataConverter_Roo_Converter {
    
    declare @type: ZoneDataConverter: @FacesConverter("com.nw.web.converter.ZoneDataConverter");
    
    public Object ZoneDataConverter.getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        Long id = Long.parseLong(value);
        return ZoneData.findZoneData(id);
    }
    
    public String ZoneDataConverter.getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof ZoneData ? ((ZoneData) value).getZoneDataId().toString() : "";
    }
    
}
