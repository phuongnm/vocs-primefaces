package com.nw.web.converter;
import com.nw.domain.TriggerDestination;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerDestination.class)
public class TriggerDestinationConverter implements Converter {
}
