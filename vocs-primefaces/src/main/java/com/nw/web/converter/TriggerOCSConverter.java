package com.nw.web.converter;
import com.nw.domain.TriggerOCS;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerOCS.class)
public class TriggerOCSConverter implements Converter {
}
