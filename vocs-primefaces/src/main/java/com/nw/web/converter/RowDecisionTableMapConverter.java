package com.nw.web.converter;
import com.nw.domain.RowDecisionTableMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = RowDecisionTableMap.class)
public class RowDecisionTableMapConverter implements Converter {
}
