package com.nw.web.converter;
import com.nw.domain.Event;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Event.class)
public class EventConverter implements Converter {
}
