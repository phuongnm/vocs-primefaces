package com.nw.web.converter;
import com.nw.domain.TriggerMsgField;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerMsgField.class)
public class TriggerMsgFieldConverter implements Converter {
}
