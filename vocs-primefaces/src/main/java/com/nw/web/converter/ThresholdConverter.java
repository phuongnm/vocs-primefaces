package com.nw.web.converter;
import com.nw.domain.Threshold;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Threshold.class)
public class ThresholdConverter implements Converter {
}
