package com.nw.web.converter;
import com.nw.domain.UnitType;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = UnitType.class)
public class UnitTypeConverter implements Converter {
}
