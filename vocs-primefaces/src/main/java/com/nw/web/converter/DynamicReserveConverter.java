package com.nw.web.converter;
import com.nw.domain.DynamicReserve;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = DynamicReserve.class)
public class DynamicReserveConverter implements Converter {
}
