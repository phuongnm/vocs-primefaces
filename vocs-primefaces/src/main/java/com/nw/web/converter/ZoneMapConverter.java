package com.nw.web.converter;
import com.nw.domain.ZoneMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ZoneMap.class)
public class ZoneMapConverter implements Converter {
}
