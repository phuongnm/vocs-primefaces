package com.nw.web.converter;
import com.nw.domain.NormValue;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = NormValue.class)
public class NormValueConverter implements Converter {
}
