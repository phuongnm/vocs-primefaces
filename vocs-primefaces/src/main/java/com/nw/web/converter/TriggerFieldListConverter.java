package com.nw.web.converter;
import com.nw.domain.TriggerFieldList;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerFieldList.class)
public class TriggerFieldListConverter implements Converter {
}
