package com.nw.web.converter;
import com.nw.domain.TriggerEvent;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerEvent.class)
public class TriggerEventConverter implements Converter {
}
