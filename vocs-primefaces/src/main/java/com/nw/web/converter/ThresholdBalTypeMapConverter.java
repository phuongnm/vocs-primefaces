package com.nw.web.converter;
import com.nw.domain.ThresholdBalTypeMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ThresholdBalTypeMap.class)
public class ThresholdBalTypeMapConverter implements Converter {
}
