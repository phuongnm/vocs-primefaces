package com.nw.web.converter;
import com.nw.domain.SortPriceComponent;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = SortPriceComponent.class)
public class SortPriceComponentConverter implements Converter {
}
