package com.nw.web.converter;
import com.nw.domain.BalType;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = BalType.class)
public class BalTypeConverter implements Converter {
}
