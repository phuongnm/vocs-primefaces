package com.nw.web.converter;
import com.nw.domain.EventActionMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = EventActionMap.class)
public class EventActionMapConverter implements Converter {
}
