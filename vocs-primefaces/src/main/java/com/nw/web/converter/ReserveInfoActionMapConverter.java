package com.nw.web.converter;
import com.nw.domain.ReserveInfoActionMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ReserveInfoActionMap.class)
public class ReserveInfoActionMapConverter implements Converter {
}
