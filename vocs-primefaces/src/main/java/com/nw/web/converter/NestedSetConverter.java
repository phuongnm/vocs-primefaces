package com.nw.web.converter;
import com.nw.domain.NestedSet;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = NestedSet.class)
public class NestedSetConverter implements Converter {
}
