package com.nw.web.converter;
import com.nw.domain.ReserveInfo;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ReserveInfo.class)
public class ReserveInfoConverter implements Converter {
}
