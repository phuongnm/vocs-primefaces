package com.nw.web.converter;
import com.nw.domain.NormalizerNormValueMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = NormalizerNormValueMap.class)
public class NormalizerNormValueMapConverter implements Converter {
}
