package com.nw.web.converter;
import com.nw.domain.DynamicReserveRateTableMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = DynamicReserveRateTableMap.class)
public class DynamicReserveRateTableMapConverter implements Converter {
}
