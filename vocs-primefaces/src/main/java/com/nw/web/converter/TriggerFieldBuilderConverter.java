package com.nw.web.converter;
import com.nw.domain.TriggerFieldBuilder;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerFieldBuilder.class)
public class TriggerFieldBuilderConverter implements Converter {
}
