package com.nw.web.converter;
import com.nw.domain.NormalizerNormParamMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = NormalizerNormParamMap.class)
public class NormalizerNormParamMapConverter implements Converter {
}
