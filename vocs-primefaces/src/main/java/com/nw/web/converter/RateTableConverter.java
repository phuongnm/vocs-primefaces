package com.nw.web.converter;
import com.nw.domain.RateTable;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = RateTable.class)
public class RateTableConverter implements Converter {
}
