package com.nw.web.converter;
import com.nw.domain.Formula;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Formula.class)
public class FormulaConverter implements Converter {
}
