package com.nw.web.converter;
import com.nw.domain.TriggerType;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerType.class)
public class TriggerTypeConverter implements Converter {
}
