package com.nw.web.converter;
import com.nw.domain.Block;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Block.class)
public class BlockConverter implements Converter {
}
