package com.nw.web.converter;
import com.nw.domain.Parameters;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Parameters.class)
public class ParametersConverter implements Converter {
}
