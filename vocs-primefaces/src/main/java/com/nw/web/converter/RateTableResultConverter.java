package com.nw.web.converter;
import com.nw.domain.RateTableResult;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = RateTableResult.class)
public class RateTableResultConverter implements Converter {
}
