package com.nw.web.converter;
import com.nw.domain.BlockRateTableMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = BlockRateTableMap.class)
public class BlockRateTableMapConverter implements Converter {
}
