package com.nw.web.converter;
import com.nw.domain.AcmBillCycle;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = AcmBillCycle.class)
public class AcmBillCycleConverter implements Converter {
}
