package com.nw.web.converter;
import com.nw.domain.PriceRateMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = PriceRateMap.class)
public class PriceRateMapConverter implements Converter {
}
