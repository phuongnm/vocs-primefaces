// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web.converter;

import com.nw.domain.ColumnDT;
import com.nw.web.converter.ColumnDTConverter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

privileged aspect ColumnDTConverter_Roo_Converter {
    
    declare @type: ColumnDTConverter: @FacesConverter("com.nw.web.converter.ColumnDTConverter");
    
    public Object ColumnDTConverter.getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        Long id = Long.parseLong(value);
        return ColumnDT.findColumnDT(id);
    }
    
    public String ColumnDTConverter.getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof ColumnDT ? ((ColumnDT) value).getColumnId().toString() : "";
    }
    
}
