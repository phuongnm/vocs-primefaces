package com.nw.web.converter;
import com.nw.domain.EventActionTypeMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = EventActionTypeMap.class)
public class EventActionTypeMapConverter implements Converter {
}
