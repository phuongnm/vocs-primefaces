package com.nw.web.converter;
import com.nw.domain.TriggerMsg;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerMsg.class)
public class TriggerMsgConverter implements Converter {
}
