package com.nw.web.converter;
import com.nw.domain.ActionPriceComponentMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ActionPriceComponentMap.class)
public class ActionPriceComponentMapConverter implements Converter {
}
