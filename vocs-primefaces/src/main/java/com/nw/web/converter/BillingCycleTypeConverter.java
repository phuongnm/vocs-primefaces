package com.nw.web.converter;
import com.nw.domain.BillingCycleType;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = BillingCycleType.class)
public class BillingCycleTypeConverter implements Converter {
}
