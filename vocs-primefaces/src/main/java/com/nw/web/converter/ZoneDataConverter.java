package com.nw.web.converter;
import com.nw.domain.ZoneData;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ZoneData.class)
public class ZoneDataConverter implements Converter {
}
