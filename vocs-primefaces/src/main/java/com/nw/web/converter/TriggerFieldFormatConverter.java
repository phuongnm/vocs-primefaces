package com.nw.web.converter;
import com.nw.domain.TriggerFieldFormat;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerFieldFormat.class)
public class TriggerFieldFormatConverter implements Converter {
}
