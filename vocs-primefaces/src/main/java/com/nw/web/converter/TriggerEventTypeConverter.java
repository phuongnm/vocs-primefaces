package com.nw.web.converter;
import com.nw.domain.TriggerEventType;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = TriggerEventType.class)
public class TriggerEventTypeConverter implements Converter {
}
