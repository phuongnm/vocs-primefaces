package com.nw.web.converter;
import com.nw.domain.SortPriceRateTableMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = SortPriceRateTableMap.class)
public class SortPriceRateTableMapConverter implements Converter {
}
