package com.nw.web.converter;
import com.nw.domain.Actions;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Actions.class)
public class ActionsConverter implements Converter {
}
