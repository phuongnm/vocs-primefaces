// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web.converter;

import com.nw.domain.AcmBillCycle;
import com.nw.web.converter.AcmBillCycleConverter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

privileged aspect AcmBillCycleConverter_Roo_Converter {
    
    declare @type: AcmBillCycleConverter: @FacesConverter("com.nw.web.converter.AcmBillCycleConverter");
    
    public Object AcmBillCycleConverter.getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        Long id = Long.parseLong(value);
        return AcmBillCycle.findAcmBillCycle(id);
    }
    
    public String AcmBillCycleConverter.getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof AcmBillCycle ? ((AcmBillCycle) value).getAcmBillCycleKey().toString() : "";
    }
    
}
