package com.nw.web.converter;
import com.nw.domain.PriceComponent;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = PriceComponent.class)
public class PriceComponentConverter implements Converter {
}
