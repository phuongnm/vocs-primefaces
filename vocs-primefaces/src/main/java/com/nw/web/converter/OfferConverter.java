package com.nw.web.converter;
import com.nw.domain.Offer;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Offer.class)
public class OfferConverter implements Converter {
}
