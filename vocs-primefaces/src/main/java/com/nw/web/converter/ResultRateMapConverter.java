package com.nw.web.converter;
import com.nw.domain.ResultRateMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ResultRateMap.class)
public class ResultRateMapConverter implements Converter {
}
