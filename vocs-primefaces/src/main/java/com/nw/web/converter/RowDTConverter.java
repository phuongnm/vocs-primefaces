package com.nw.web.converter;
import com.nw.domain.RowDT;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = RowDT.class)
public class RowDTConverter implements Converter {
}
