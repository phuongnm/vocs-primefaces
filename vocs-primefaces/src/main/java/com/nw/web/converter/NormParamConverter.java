package com.nw.web.converter;
import com.nw.domain.NormParam;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = NormParam.class)
public class NormParamConverter implements Converter {
}
