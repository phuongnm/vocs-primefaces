package com.nw.web.converter;
import com.nw.domain.BillingCycle;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = BillingCycle.class)
public class BillingCycleConverter implements Converter {
}
