// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web.converter;

import com.nw.web.converter.NormParamConverter;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect NormParamConverter_Roo_Configurable {
    
    declare @type: NormParamConverter: @Configurable;
    
}
