package com.nw.web.converter;
import com.nw.domain.Normalizer;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Normalizer.class)
public class NormalizerConverter implements Converter {
}
