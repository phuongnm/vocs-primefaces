package com.nw.web.converter;
import com.nw.domain.DecisionTable;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = DecisionTable.class)
public class DecisionTableConverter implements Converter {
}
