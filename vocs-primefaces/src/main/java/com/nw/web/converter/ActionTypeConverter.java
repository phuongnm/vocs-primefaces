package com.nw.web.converter;
import com.nw.domain.ActionType;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ActionType.class)
public class ActionTypeConverter implements Converter {
}
