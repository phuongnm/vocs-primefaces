package com.nw.web.converter;
import com.nw.domain.GeoHomeZone;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = GeoHomeZone.class)
public class GeoHomeZoneConverter implements Converter {
}
