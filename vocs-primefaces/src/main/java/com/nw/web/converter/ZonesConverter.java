package com.nw.web.converter;
import com.nw.domain.Zones;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Zones.class)
public class ZonesConverter implements Converter {
}
