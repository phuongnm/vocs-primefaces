package com.nw.web.converter;
import com.nw.domain.OfferActionMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = OfferActionMap.class)
public class OfferActionMapConverter implements Converter {
}
