package com.nw.web.converter;
import com.nw.domain.OfferParameterMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = OfferParameterMap.class)
public class OfferParameterMapConverter implements Converter {
}
