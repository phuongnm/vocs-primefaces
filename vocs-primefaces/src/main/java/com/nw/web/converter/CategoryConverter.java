package com.nw.web.converter;
import com.nw.domain.Category;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = Category.class)
public class CategoryConverter implements Converter {
}
