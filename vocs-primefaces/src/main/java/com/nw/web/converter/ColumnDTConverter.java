package com.nw.web.converter;
import com.nw.domain.ColumnDT;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ColumnDT.class)
public class ColumnDTConverter implements Converter {
}
