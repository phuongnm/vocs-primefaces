package com.nw.web.converter;
import com.nw.domain.ColumnDecisionTableMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = ColumnDecisionTableMap.class)
public class ColumnDecisionTableMapConverter implements Converter {
}
