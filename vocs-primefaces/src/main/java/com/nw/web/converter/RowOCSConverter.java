package com.nw.web.converter;
import com.nw.domain.RowOCS;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = RowOCS.class)
public class RowOCSConverter implements Converter {
}
