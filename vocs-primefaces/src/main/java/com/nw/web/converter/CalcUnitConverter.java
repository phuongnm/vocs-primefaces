package com.nw.web.converter;
import com.nw.domain.CalcUnit;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = CalcUnit.class)
public class CalcUnitConverter implements Converter {
}
