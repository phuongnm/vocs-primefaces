// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.nw.web.converter;

import com.nw.domain.TriggerEvent;
import com.nw.web.converter.TriggerEventConverter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

privileged aspect TriggerEventConverter_Roo_Converter {
    
    declare @type: TriggerEventConverter: @FacesConverter("com.nw.web.converter.TriggerEventConverter");
    
    public Object TriggerEventConverter.getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        Long id = Long.parseLong(value);
        return TriggerEvent.findTriggerEvent(id);
    }
    
    public String TriggerEventConverter.getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof TriggerEvent ? ((TriggerEvent) value).getTriggerEventId().toString() : "";
    }
    
}
