package com.nw.web.converter;
import com.nw.domain.PriceComponentBlockMap;
import javax.faces.convert.Converter;
import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

@RooJsfConverter(entity = PriceComponentBlockMap.class)
public class PriceComponentBlockMapConverter implements Converter {
}
