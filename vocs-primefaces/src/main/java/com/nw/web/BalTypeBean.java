package com.nw.web;
import com.nw.domain.BalType;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = BalType.class, beanName = "balTypeBean")
public class BalTypeBean {
}
