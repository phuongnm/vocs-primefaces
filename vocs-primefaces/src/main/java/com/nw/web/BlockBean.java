package com.nw.web;
import com.nw.domain.Block;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = Block.class, beanName = "blockBean")
public class BlockBean {
}
