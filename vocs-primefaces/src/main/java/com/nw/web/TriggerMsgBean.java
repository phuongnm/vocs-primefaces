package com.nw.web;
import com.nw.domain.TriggerMsg;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerMsg.class, beanName = "triggerMsgBean")
public class TriggerMsgBean {
}
