package com.nw.web;
import com.nw.domain.OfferActionMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = OfferActionMap.class, beanName = "offerActionMapBean")
public class OfferActionMapBean {
}
