package com.nw.web;
import com.nw.domain.ReserveInfoActionMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = ReserveInfoActionMap.class, beanName = "reserveInfoActionMapBean")
public class ReserveInfoActionMapBean {
}
