package com.nw.web;
import com.nw.domain.RowDT;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = RowDT.class, beanName = "rowDTBean")
public class RowDTBean {
}
