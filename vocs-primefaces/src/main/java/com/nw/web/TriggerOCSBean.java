package com.nw.web;
import com.nw.domain.TriggerOCS;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = TriggerOCS.class, beanName = "triggerOCSBean")
public class TriggerOCSBean {
}
