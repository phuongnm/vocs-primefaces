package com.nw.web;
import com.nw.domain.EventActionTypeMap;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = EventActionTypeMap.class, beanName = "eventActionTypeMapBean")
public class EventActionTypeMapBean {
}
