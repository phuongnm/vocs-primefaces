package com.nw.web;
import com.nw.domain.AcmBillCycle;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

@RooSerializable
@RooJsfManagedBean(entity = AcmBillCycle.class, beanName = "acmBillCycleBean")
public class AcmBillCycleBean {
}
